#define DOT 300
#define LED 7
//time spans msg[ind]*DOT
byte msg[]={1,1,1,1,1,3, 3,1,3,1,3,3, 1,1,1,1,1,6};
byte len=18;
byte state=1;
void setup() {
  pinMode(LED,OUTPUT);
}

void loop() {
  for(byte i=0;i<len;i++){
    digitalWrite(LED,state);
    state=1-state;
    delay(msg[i]*DOT);
  }
}
