#include "SBUS.h"
#define LVAL 180
#define HVAL 1800
// a SBUS object, which is on hardware
// serial port 1
SBUS x8r(Serial1);

// channel, fail safe, and lost frames data
uint16_t channels[16];
byte pwmchannels[16];
byte led_pins[]={7,8,9,10,13};
bool failSafe;
bool lostFrame;
byte chstate=0;
long prevmil=0;
void setup() {
  for(byte i=0;i<5;i++)  
    pinMode(led_pins[i], OUTPUT);
  // begin the SBUS communication
  x8r.begin();
}

void loop() {
  // look for a good SBUS packet from the receiver
  if(x8r.read(&channels[0], &failSafe, &lostFrame)){    
    chstate=map(constrain(channels[9],LVAL,HVAL),LVAL,HVAL,0,255)>128?255-chstate:chstate;    
    analogWrite(9,map(constrain(channels[8],LVAL,HVAL),LVAL,HVAL,0,255));
    analogWrite(10,chstate);     
  }
}
